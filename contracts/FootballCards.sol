// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@chainlink/contracts/src/v0.6/VRFConsumerBase.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract FootballCards is ERC721, VRFConsumerBase, Ownable {
    uint256 public tokenCounter;
    bytes32 internal keyhash;
    uint256 internal fee;
    enum Perk1 {
        NONE,
        SPEEDSTER,
        PHYSICAL,
        BALANCED
    }
    enum Perk2 {
        NONE,
        INTELIGENT,
        HARDWORKING,
        BALANCED
    }
    enum Perk3 {
        NONE,
        WATCHDOG,
        ORGANIZER,
        DEFENSIVE_ANCHOR
    }
    enum Boost1 {
        NONE,
        SPEEDBOOST,
        SHOOTINGBOOST
    }
    enum Boost2 {
        NONE,
        STRENGTHBOOST,
        PASSBOOST,
        CONTROLBOOST
    }
    enum Boost3 {
        NONE,
        DEFENSEBOOST,
        SUPERBOOST
    }
    enum Level {
        NEIGHBORHOOD,
        CITY,
        STATE,
        COUNTRY,
        WORLD
    }
    enum DominantSide {
        LEFT,
        RIGHT
    }
    enum InjuryProne {
        NONE,
        LIGHT,
        MEDIUM,
        SEVERE
    }
    struct playerChar {
        uint256 height;
        uint256 weight;
        Level level;
        DominantSide dominantSide;
        InjuryProne ijuryProne;
    }
    struct playerPerk {
        Perk1 perk1;
        Perk2 perk2;
        Perk3 perk3;
        Boost1 boost1;
        Boost2 boost2;
        Boost3 boost3;
    }
    struct playerCard {
        string name;
        uint256 speed;
        uint256 acceleration;
        uint256 pass;
        uint256 shooting;
        uint256 defense;
        uint256 control;
        playerPerk perks;
        playerChar char;
    }

    mapping(bytes32 => address) public requestIdToSender;
    mapping(bytes32 => string) public requestIdToName;
    mapping(uint256 => playerCard) public tokenIdToPlayers;
    mapping(uint256 => playerPerk) public tokenIdToPlayersPerks;
    mapping(uint256 => playerChar) public tokenIdToPlayerChar;

    constructor(
        bytes32 _keyhash,
        address _VRFCoordinator,
        address _linkToken,
        uint256 _fee
    )
        public
        VRFConsumerBase(_VRFCoordinator, _linkToken)
        ERC721("Football Cards Test", "FC")
    {
        tokenCounter = 0;
        keyhash = _keyhash;
        fee = _fee;
    }

    function createFootballCard(string memory name) public onlyOwner {
        bytes32 requestId = requestRandomness(keyhash, fee);
        requestIdToSender[requestId] = msg.sender;
        requestIdToName[requestId] = name;
    }

    function fulfillRandomness(bytes32 requestId, uint256 randomNumber)
        internal
        override
    {
        uint256 newId = tokenCounter;
        address owner = requestIdToSender[requestId];
        string memory name = requestIdToName[requestId];
        uint256 speed = (randomNumber % 41) + 60;
        uint256 acceleration = ((randomNumber / 10) % 41) + 60;
        uint256 pass = ((randomNumber / 100) % 41) + 60;
        uint256 shooting = ((randomNumber / 1000) % 41) + 60;
        uint256 defense = ((randomNumber / 10000) % 41) + 60;
        uint256 control = ((randomNumber / 100000) % 41) + 60;
        getPerks(randomNumber);
        getChar(randomNumber);

        tokenIdToPlayers[tokenCounter] = playerCard(
            name,
            speed,
            acceleration,
            pass,
            shooting,
            defense,
            control,
            tokenIdToPlayersPerks[tokenCounter],
            tokenIdToPlayerChar[tokenCounter]
        );

        _safeMint(owner, newId);
        tokenCounter = tokenCounter + 1;
    }

    function getChar(uint256 randomNumber) internal {
        uint256 height = ((randomNumber / 1000000000000) % 40) + 160;
        uint256 weight = ((((randomNumber / 10000000000000) % 50) + height) /
            25) * 10;
        Level level;
        DominantSide dominantSide;
        InjuryProne injuryProne;

        if (
            (((randomNumber / 100000000000000) % 101) >= 0) &&
            (((randomNumber / 100000000000000) % 101) <= 28)
        ) {
            level = Level(0);
        } else if (
            (((randomNumber / 100000000000000) % 101) >= 29) &&
            (((randomNumber / 100000000000000) % 101) <= 51)
        ) {
            level = Level(1);
        } else if (
            (((randomNumber / 100000000000000) % 101) >= 52) &&
            (((randomNumber / 100000000000000) % 101) <= 70)
        ) {
            level = Level(2);
        } else if (
            (((randomNumber / 100000000000000) % 101) >= 71) &&
            (((randomNumber / 100000000000000) % 101) <= 88)
        ) {
            level = Level(3);
        } else if (
            (((randomNumber / 100000000000000) % 101) >= 89) &&
            (((randomNumber / 100000000000000) % 101) <= 100)
        ) {
            level = Level(5);
        }

        if (
            (((randomNumber / 1000000000000000) % 101) >= 0) &&
            (((randomNumber / 1000000000000000) % 101) <= 76)
        ) {
            dominantSide = DominantSide(1);
        } else {
            dominantSide = DominantSide(0);
        }

        if (
            (((randomNumber / 10000000000000000) % 101) >= 0) &&
            (((randomNumber / 10000000000000000) % 101) <= 10)
        ) {
            injuryProne = InjuryProne(0);
        } else if (
            (((randomNumber / 1000000000000000) % 101) >= 11) &&
            (((randomNumber / 1000000000000000) % 101) <= 60)
        ) {
            injuryProne = InjuryProne(1);
        } else if (
            (((randomNumber / 1000000000000000) % 101) >= 61) &&
            (((randomNumber / 1000000000000000) % 101) <= 89)
        ) {
            injuryProne = InjuryProne(2);
        } else {
            injuryProne = InjuryProne(3);
        }

        tokenIdToPlayerChar[tokenCounter] = playerChar(
            height,
            weight,
            level,
            dominantSide,
            injuryProne
        );
    }

    function getPerks(uint256 randomNumber) internal {
        Perk1 perk1;
        Perk2 perk2;
        Perk3 perk3;
        Boost1 boost1;
        Boost2 boost2;
        Boost3 boost3;

        if (((randomNumber / 1000000) % 15) < 5)
            perk1 = Perk1((randomNumber % 3) + 1);
        else perk1 = Perk1(0);
        if (((randomNumber / 10000000) % 15) < 5)
            perk2 = Perk2((randomNumber % 3) + 1);
        else perk2 = Perk2(0);
        if (((randomNumber / 100000000) % 15) < 5)
            perk3 = Perk3((randomNumber % 3) + 1);
        else perk3 = Perk3(0);
        if (((randomNumber / 1000000000) % 15) < 5)
            boost1 = Boost1((randomNumber % 2) + 1);
        else boost1 = Boost1(0);
        if (((randomNumber / 10000000000) % 15) < 5)
            boost2 = Boost2((randomNumber % 3) + 1);
        else boost2 = Boost2(0);
        if (((randomNumber / 100000000000) % 15) < 5)
            boost3 = Boost3((randomNumber % 2) + 1);
        else boost3 = Boost3(0);

        tokenIdToPlayersPerks[tokenCounter] = playerPerk(
            perk1,
            perk2,
            perk3,
            boost1,
            boost2,
            boost3
        );
    }

    function getPlayer(uint256 position)
        public
        view
        returns (playerCard memory)
    {
        return tokenIdToPlayers[position];
    }

    function updatePlayer(uint256 tokenId, uint256[] memory upgrades)
        public
        onlyOwner
    {
        playerCard memory player = tokenIdToPlayers[tokenId];
        player.speed = player.speed + upgrades[0];
        player.acceleration = player.acceleration + upgrades[1];
        player.pass = player.pass + upgrades[2];
        player.shooting = player.shooting + upgrades[3];
        player.defense = player.defense + upgrades[4];
        player.control = player.control + upgrades[5];
        tokenIdToPlayers[tokenId] = player;
    }

    function setTokenUri(uint256 tokenId, string memory _tokenURI)
        public
        onlyOwner
    {
        _setTokenURI(tokenId, _tokenURI);
    }
}
