from brownie import FootballCards, network
from click import prompt
from scripts.helpfull_scripts import getAccount
from scripts.create_metadata import writeMetadata
import os


def updateFootballCard():
    account = getAccount()
    token_id = prompt("Enter the tokenId you'd like to update:")
    upgrades = []
    for i in range(6):
        temp = prompt("Enter the attribute {} you'd like to upgrade to:".format(i))
        upgrades.append(temp)

    football_cards = FootballCards[-1]
    tx = football_cards.updatePlayer(token_id, upgrades, {"from": account})
    tx.wait(1)
    updateMetadata(token_id)


def updateMetadata(token_id):
    metadata_file_name = "./metadata/{}/{}.json".format(network.show_active(), token_id)
    uri_file_name = "./metadata/{}/uris/{}.json".format(network.show_active(), token_id)
    os.remove(metadata_file_name)
    os.remove(uri_file_name)
    football_cards = FootballCards[-1]
    writeMetadata(football_cards, football_cards.tokenCounter())


def main():
    updateFootballCard()
