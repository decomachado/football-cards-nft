from email import header
from lib2to3.pgen2 import token
from brownie import network, FootballCards
from metadata import sample_metadata
from pathlib import Path
import os
import requests
import json
import time
import copy
from scripts.helpfull_scripts import getPerks, getChar


def main():
    print("Working on " + network.show_active())
    football_cards = FootballCards[-1]
    number_of_players = football_cards.tokenCounter()
    print("The number of players created is " + str(number_of_players))
    writeMetadata(football_cards, number_of_players)


def writeMetadata(nft_contract, token_ids):
    for token_id in range(token_ids):
        player_metadata = copy.deepcopy(sample_metadata.METADATA_TEMPLATE)
        uri_metadata = sample_metadata.token_uri_template
        metadata_file_name = (
            "./metadata/{}/".format(network.show_active()) + str(token_id) + ".json"
        )
        uri_file_name = (
            "./metadata/{}/uris/".format(network.show_active())
            + str(token_id)
            + ".json"
        )
        player = nft_contract.getPlayer(token_id)
        if Path(metadata_file_name).exists():
            print(
                "{} already found, delete it to overwrite!".format(metadata_file_name)
            )
        else:
            print("Creating metadata file " + metadata_file_name)

            player_metadata["name"] = player[0]
            player_metadata["description"] = "An amazing Football player!"
            player_metadata["image"] = uploadImageToPinata(token_id)

            for i in range(0, 6):
                player_metadata["attributes"][i]["value"] = player[i + 1]

            player_metadata = getPerks(token_id, player_metadata)
            player_metadata = getChar(token_id, player_metadata)

            with open(metadata_file_name, "w") as file:
                json.dump(player_metadata, file)

            token_uri = uploadJsonToPinata(metadata_file_name)
            uri_metadata["uri"] = token_uri

            if Path(uri_file_name).exists():
                print("{} already exists".format(uri_file_name))
            else:
                with open(uri_file_name, "w") as file:
                    json.dump(uri_metadata, file)


def uploadJsonToPinata(filepath):
    print("Uploading JSON to IPFS")
    PINATA_BASE_URL = "https://api.pinata.cloud/"
    endpoint = "pinning/pinJSONToIPFS"
    headers = {
        "pinata_api_key": os.getenv("PINATA_API_KEY"),
        "pinata_secret_api_key": os.getenv("PINATA_SECRET_API_KEY"),
    }
    filename = filepath.split("/")[-1:][0]
    file = open(filepath, "r")
    json_file = json.load(file)
    response = requests.post(
        PINATA_BASE_URL + endpoint,
        json=json_file,
        headers=headers,
    ).json()
    file.close()
    tokenUri = "https://ipfs.io/ipfs/{}?filename={}".format(
        response["IpfsHash"], filename
    )
    print("Uploaded JSON to {}".format(tokenUri))
    return tokenUri


def uploadImageToPinata(token_id):
    print("Uploading image to Pinata")
    PINATA_BASE_URL = "https://api.pinata.cloud/"
    endpoint = "pinning/pinFileToIPFS"
    filepath = "img/{}.jpeg".format(token_id)
    filename = filepath.split("/")[-1:][0]
    headers = {
        "pinata_api_key": os.getenv("PINATA_API_KEY"),
        "pinata_secret_api_key": os.getenv("PINATA_SECRET_API_KEY"),
    }

    with Path(filepath).open("rb") as fp:
        image_binary = fp.read()
        response = requests.post(
            PINATA_BASE_URL + endpoint,
            files={"file": (filename, image_binary)},
            headers=headers,
        ).json()

        imageUri = "https://ipfs.io/ipfs/{}?filename={}".format(
            response["IpfsHash"], filename
        )
        print("Uploaded image to {}".format(imageUri))
        return imageUri
