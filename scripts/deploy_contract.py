from brownie import FootballCards, network, accounts, config
from scripts.helpfull_scripts import getAccount, getContract


def main():
    account = getAccount()
    footbal_cards = FootballCards.deploy(
        config["networks"][network.show_active()]["keyhash"],
        getContract("vrf_coordinator").address,
        getContract("link_token").address,
        config["networks"][network.show_active()]["fee"],
        {"from": account},
        publish_source=config["networks"][network.show_active()].get("verify", False),
    )
    print("Deployed Football Cards Contract!")
    return footbal_cards
