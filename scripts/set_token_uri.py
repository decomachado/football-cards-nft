from brownie import FootballCards, network
import json
from scripts.helpfull_scripts import getAccount


def main():
    print("Working on " + network.show_active())
    account = getAccount()
    football_cards = FootballCards[-1]
    for i in range(football_cards.tokenCounter()):
        uri_file_path = (
            "./metadata/{}/uris/".format(network.show_active()) + str(i) + ".json"
        )
        with open(uri_file_path, "r") as file:
            tokenURI = json.load(file)["uri"]
            if football_cards.tokenURI(i) == tokenURI:
                print("URI Already up to date!")
            else:
                tx = football_cards.setTokenUri(i, tokenURI, {"from": account})
                tx.wait(1)
                print("TokenURI for id {} set!".format(i))
