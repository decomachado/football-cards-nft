from lib2to3.pgen2 import token
from brownie import (
    FootballCards,
    config,
    network,
    interface,
    accounts,
    Contract,
    LinkToken,
    MockOracle,
    VRFCoordinatorMock,
)
import time

NON_FORKED_LOCAL_BLOCKCHAIN_ENVIRONMENTS = ["hardhat", "development", "ganache"]
LOCAL_BLOCKCHAIN_ENVIRONMENTS = NON_FORKED_LOCAL_BLOCKCHAIN_ENVIRONMENTS + [
    "mainnet-fork",
    "binance-fork",
    "matic-fork",
]

contract_to_mock = {
    "link_token": LinkToken,
    "vrf_coordinator": VRFCoordinatorMock,
    "oracle": MockOracle,
}

perk1 = {
    0: None,
    1: "SPEEDSTER",
    2: "PHYSICAL",
    3: "BALANCED",
}

perk2 = {
    0: None,
    1: "INTELIGENT",
    2: "HARDWORKING",
    3: "BALANCED",
}

perk3 = {
    0: None,
    1: "WATCHDOG",
    2: "ORGANIZER",
    3: "DEFENSIVE_ANCHOR",
}

boost1 = {
    0: None,
    1: "SPEEDBOOST",
    2: "SHOOTINGBOOST",
}

boost2 = {
    0: None,
    1: "STRENGTHBOOST",
    2: "PASSBOOST",
    3: "CONTROLBOOST",
}

boost3 = {
    0: None,
    1: "DEFENSEBOOST",
    2: "SUPERBOOST",
}

level = {
    0: "NEIGHBORHOOD",
    1: "CITY",
    2: "STATE",
    3: "COUNTRY",
    4: "WORLD",
}

dominant_side = {
    0: "LEFT",
    1: "RIGHT",
}

injury_prone = {
    0: None,
    1: "LIGHT",
    2: "MEDIUM",
    3: "SEVERE",
}


def getChar(token_id, playerMetadata):
    football_cards = FootballCards[-1]
    player = football_cards.getPlayer(token_id)
    char = player[len(player) - 1]
    playerMetadata["attributes"].append(
        {
            "trait_type": "height",
            "value": char[0],
            "max_value": 200,
        }
    )
    playerMetadata["attributes"].append(
        {
            "trait_type": "weight",
            "value": char[1],
        }
    )

    char_index = int(char[2])

    status = {
        "trait_type": "level",
        "value": level[char_index],
    }

    playerMetadata["attributes"].append(status)

    char_index = int(char[3])
    dominant = {
        "trait_type": "dominant side",
        "value": dominant_side[char_index],
    }
    playerMetadata["attributes"].append(dominant)

    char_index = int(char[4])
    injury = {"trait_type": "injury prone", "value": injury_prone[char_index]}
    playerMetadata["attributes"].append(injury)

    return playerMetadata


def getPerks(token_id, playerMetadata):
    football_cards = FootballCards[-1]
    players = football_cards.getPlayer(token_id)
    perks = players[len(players) - 2]

    perk_index = int(perks[0])

    if perk_index != 0:
        perk = {
            "trait_type": perk1[perk_index],
            "value": 5,
            "display_type": "boost_percentage",
        }
        playerMetadata["attributes"].append(perk)

    perk_index = int(perks[1])

    if perk_index != 0:
        perk = {
            "trait_type": perk2[perk_index],
            "value": 5,
            "display_type": "boost_percentage",
        }
        playerMetadata["attributes"].append(perk)

    perk_index = int(perks[2])

    if perk_index != 0:
        perk = {
            "trait_type": perk3[perk_index],
            "value": 5,
            "display_type": "boost_percentage",
        }
        playerMetadata["attributes"].append(perk)

    perk_index = int(perks[3])

    if perk_index != 0:
        boost = {
            "trait_type": boost1[perk_index],
            "value": 10,
            "display_type": "boost_number",
        }
        playerMetadata["attributes"].append(boost)

    perk_index = int(perks[4])

    if perk_index != 0:
        boost = {
            "trait_type": boost2[perk_index],
            "value": 10,
            "display_type": "boost_number",
        }
        playerMetadata["attributes"].append(boost)

    perk_index = int(perks[5])

    if perk_index != 0:
        boost = {
            "trait_type": boost3[perk_index],
            "value": 10,
            "display_type": "boost_number",
        }
        playerMetadata["attributes"].append(boost)

    return playerMetadata


def getAccount(index=None, id=None):
    if index:
        return accounts[index]
    if id:
        return accounts.load(id)
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        return accounts[0]
    return accounts.add(config["wallets"]["from_key"])


def fundWithLink(
    contract_address, amoount_of_collectibles, account=None, link_token=None
):  # 0.1 LINK
    account = account if account else getAccount()
    link_token = link_token if link_token else getContract("link_token")

    current_balance = link_token.balanceOf(contract_address)
    amount = config["networks"][network.show_active()]["fee"]
    if current_balance != 0:
        fee = amount * amoount_of_collectibles
        if fee < current_balance:
            fee = 0
        else:
            fee = fee - current_balance
    else:
        fee = amount * amoount_of_collectibles
    if fee == 0:
        print("Fee already paid")
        return 0
    else:
        tx = link_token.transfer(contract_address, fee, {"from": account})
        tx.wait(1)
        time.sleep(10)
        print("Funded contract!")
        return tx


def getContract(contract_name):
    """If you want to use this function, go to the brownie config and add a new entry for
    the contract that you want to be able to 'get'. Then add an entry in the in the variable 'contract_to_mock'.
    You'll see examples like the 'link_token'.
        This script will then either:
            - Get a address from the config
            - Or deploy a mock to use for a network that doesn't have it
        Args:
            contract_name (string): This is the name that is refered to in the
            brownie config and 'contract_to_mock' variable.
        Returns:
            brownie.network.contract.ProjectContract: The most recently deployed
            Contract of the type specificed by the dictonary. This could be either
            a mock or the 'real' contract on a live network.
    """
    contract_type = contract_to_mock[contract_name]
    if network.show_active() in NON_FORKED_LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        if len(contract_type) <= 0:
            deploy_mocks()
        contract = contract_type[-1]
    else:
        try:
            contract_address = config["networks"][network.show_active()][contract_name]
            contract = Contract.from_abi(
                contract_type._name, contract_address, contract_type.abi
            )
        except KeyError:
            print(
                f"{network.show_active()} address not found, perhaps you should add it to the config or deploy mocks?"
            )
            print(
                f"brownie run scripts/deploy_mocks.py --network {network.show_active()}"
            )
    return contract


def deploy_mocks(decimals=18, initial_value=2000):
    """
    Use this script if you want to deploy mocks to a testnet
    """
    print(f"The active network is {network.show_active()}")
    print("Deploying Mocks...")
    account = getAccount()
    print("Deploying Mock Link Token...")
    link_token = LinkToken.deploy({"from": account})
    print("Deploying Mock VRFCoordinator...")
    mock_vrf_coordinator = VRFCoordinatorMock.deploy(
        link_token.address, {"from": account}
    )
    print(f"Deployed to {mock_vrf_coordinator.address}")

    print("Deploying Mock Oracle...")
    mock_oracle = MockOracle.deploy(link_token.address, {"from": account})
    print(f"Deployed to {mock_oracle.address}")
    print("Mocks Deployed!")
