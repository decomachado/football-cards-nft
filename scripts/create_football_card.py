from pydoc import doc
from brownie import FootballCards
import time

from click import prompt
from scripts.helpfull_scripts import fundWithLink, getAccount


def main():
    account = getAccount()
    football_cards = FootballCards[-1]
    players_to_create = []
    while True:
        name = prompt(
            "Enter the name of the player you want to create or quit to start creating!"
        )
        if name == "quit":
            break
        players_to_create.append(name)

    fundWithLink(football_cards, len(players_to_create))
    if len(players_to_create) != 0:
        for player in players_to_create:
            tx = football_cards.createFootballCard(player, {"from": account})
            tx.wait(1)
            time.sleep(10)
        time.sleep(240)

    for i in range(0, football_cards.tokenCounter()):
        print(football_cards.getPlayer(i))
